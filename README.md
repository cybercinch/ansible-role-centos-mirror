# Ansible Role: CentOS Mirror Repository

[![Build Status](https://travis-ci.org/geerlingguy/ansible-role-repo-epel.svg?branch=master)](https://travis-ci.org/geerlingguy/ansible-role-repo-epel)

Configures the baseurl for all CentOS package repositories.

## Requirements

This role only is needed/runs on CentOS.

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    centos_mirror_url: http://ucmirror.canterbury.ac.nz/linux/CentOS

The CentOS Mirror Url.

## Dependencies

None.

## Example Playbook

    - hosts: servers
      roles:
        - role: centos-mirror

## License

MIT / BSD

## Author Information

This role was created in 2019 by [Aaron Guise](https://www.guise.net.nz/).
